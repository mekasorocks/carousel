package de.mekaso.vaadin.addons;

import com.vaadin.flow.templatemodel.TemplateModel;

public interface CarouselModel extends TemplateModel {
	public void setCellCount(int cellCount);
	public void setSelectedIndex(int selectedIndex);
	public int getSelectedComponentIndex();
	public void setIsHorizontal(boolean horizontal);
	public void setWidth(String width);
	public void setHeight(String height);
	public void setBorder(boolean border);
	public void setAutoplay(boolean autoplay);
	public void setDuration(int duration);
}
