import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';
import {GestureEventListeners} from '@polymer/polymer/lib/mixins/gesture-event-listeners.js';
import '@polymer/iron-icon/iron-icon.js';

class Carousel extends GestureEventListeners(PolymerElement) {
	static get template() {
        return html`
        	<style>
		    	.scene {
				  position: relative;
				  min-width: 400px;
				  min-height: 250px;
				  margin: 40px auto 10px auto;
				  perspective: 1000px;
				}
				.bordered {
					border: 1px solid #CCC;
				}
				.carousel {
				  width: 100%;
				  height: 100%;
				  position: absolute;
				  transform-style: preserve-3d;
				  transition: transform 1s;
				}
				::slotted(div.carousel__cell) {
        			position: absolute;
        			width: calc(100% - 20px);
        			height: calc(100% - 20px);
        			left: 10px;
        			top: 10px;
        			border: 1px solid #EFEFEF;
				  	display: -ms-flexbox;
        			display: -webkit-flex;
        			display: flex;
        			-ms-flex-align: center;
        			-webkit-align-items: center;
        			-webkit-box-align: center;
        			align-items: center;
        			justify-content: center;
				}
				.carousel-navi-horizontal {
					height:100%;
        			position: absolute;
					display: -ms-flexbox;
        			display: -webkit-flex;
        			display: flex;
        			-ms-flex-align: center;
        			-webkit-align-items: center;
        			-webkit-box-align: center;
        			align-items: center;
        			justify-content: center;
				}
				.carousel-navi-horizontal-prev {
        			float:left;
					left: -35px;
				}
				.carousel-navi-horizontal-next {
        			float:right;
					right: -35px;
				}
				.carousel-navi-vertical {
					width: 100%;
					text-align: center;
					position: absolute;
				}
				.carousel-navi-vertical-next {
					top: -35px;
				}
				.carousel-navi-vertical-prev {
					bottom: -35px;
				}
		  	</style>

            <div class="scene" cell-count$="[[cellCount]]" selected-component-index$="{{selectedComponentIndex}}">
				<div class="carousel" id="carousel" on-track="handleSwipe">
			    	<slot></slot>
			  	</div>
			  	<div class$="[[naviPrev]]">
			  		<iron-icon id="nextbutton" icon="[[iconPrev]]" on-click="prev"></iron-icon>
			  	</div>
			  	<div class$="[[naviNext]]">
	    			<iron-icon id="prevbutton" icon="[[iconNext]]" on-click="next"></iron-icon>
	    		</div>
			</div>`;
    }
    static get is() {return 'mekaso-carousel'}
    
    static get properties() {
        return {
        	carousel: {
        		type: Object,
        		notify: false,
        		readOnly: false
        	},
        	cells: {
        		type: Array,
        		notify: false,
        		readOnly: false
        	},
        	cellCount: {
	            type: Number,
	            notify: false,
	            readOnly: false
        	},
        	selectedIndex: {
        		type: Number,
        		value: 0,
	            notify: false,
	            readOnly: false,
                observer: '_onCenterStageChanged'
        	},
        	selectedComponentIndex: {
        	    type: Number,
                value: 0,
                notify: true,
                readOnly: false
        	},
        	cellWidth: {
        		type: Number,
	            notify: false,
	            readOnly: false
        	},
        	cellHeight: {
        		type: Number,
	            notify: false,
	            readOnly: false
        	},
        	isHorizontal: {
        		type: Boolean,
        		value: true,
        		notify: false,
	            readOnly: false,
	            observer: '_onOrienationChanged'
        	},
        	rotateFn: {
        		type: String,
        		value: 'rotateY',
        		notify: false,
	            readOnly: false
        	},
        	radius: {
        		type: Number,
	            notify: false,
	            readOnly: false
        	},
        	theta: {
        		type: Number,
	            notify: false,
	            readOnly: false
        	},
        	naviStyle: {
        		type: String,
        		notify: false,
	            readOnly: false
        	},
        	iconPrev: {
        		type: String,
        		notify: false,
	            readOnly: false
        	},
        	iconNext: {
        		type: String,
        		notify: false,
	            readOnly: false
        	},
        	naviPrev: {
        		type: String,
        		notify: false,
	            readOnly: false
        	},
        	naviNext: {
        		type: String,
        		notify: false,
	            readOnly: false
        	}, 
        	width : {
        		type: String,
        		notify: false,
	            readOnly: false
        	},
        	height : {
        		type: String,
        		notify: false,
	            readOnly: false
        	},
        	border: {
        		type: Boolean,
        		value: false,
        		notify: false,
	            readOnly: false
        	},
        	autoplay: {
        		type: Boolean,
        		value: false,
        		notify: false,
	            readOnly: false
        	},
        	duration: {
        		type: Number,
        		value: 5000,
	            notify: false,
	            readOnly: false
        	},
        	autoInterval: {
        		type: Number,
        		value: 0,
	            notify: false,
	            readOnly: false
        	}
        }
    }
    // Custom methods
    next() {
        this.selectedIndex++;
    	this.rotate();
    	this.selectedComponentIndex = this.selectedIndex % this.cellCount;
    	if (this.selectedComponentIndex < 0) {
            this.selectedComponentIndex = this.cellCount + this.selectedComponentIndex;
        }
    }
    prev() {
        this.selectedIndex--;
    	this.rotate();
        this.selectedComponentIndex = this.selectedIndex % this.cellCount;
        if (this.selectedComponentIndex < 0) {
            this.selectedComponentIndex = this.cellCount + this.selectedComponentIndex;
        }
    }
    handleSwipe(event) {
    	switch(event.detail.state) {
    		case 'end':
	            if (this.isHorizontal) {
	            	let horizontalSwipe = event.detail.dx;
	            	let minimumSwipeDistance = this.cellWidth / 3;
	            	if (horizontalSwipe > 0 && minimumSwipeDistance < horizontalSwipe) {
	            		this.next();
	            	} else if (horizontalSwipe < 0 && minimumSwipeDistance < (horizontalSwipe * -1)) {
	            		this.prev();
	            	}
	            } else {
	            	let minimumSwipeDistance = this.cellHeight / 3;
	            	let verticalSwipe = event.detail.dy;
	            	if (verticalSwipe > 0 && minimumSwipeDistance < verticalSwipe) {
	            		this.next();
	            	} else if (verticalSwipe < 0 && minimumSwipeDistance < (verticalSwipe * -1)) {
	            		this.prev();
	            	}
	            }
	            break;
    	}
    }
    rotate() {
    	var angle = this.theta * this.selectedIndex * -1;
    	this.carousel.style.transform = 'translateZ(' + -this.radius + 'px) ' + this.rotateFn + '(' + angle + 'deg)';
    }
    initCarousel() {
    	this.theta = 360 / this.cellCount;
    	var cellSize = this.isHorizontal ? this.cellWidth : this.cellHeight;
    	this.radius = Math.round((cellSize / 2) / Math.tan(Math.PI / this.cellCount));
    	for (var i=0; i < this.cells.length; i++) {
    		var cell = this.cells[i];
    	    if (i < this.cellCount ) { // visible cell
    	    	cell.style.opacity = 1;
    	    	var cellAngle = this.theta * i;
    	    	cell.style.transform = this.rotateFn + '(' + cellAngle + 'deg) translateZ(' + this.radius + 'px)';
    	    } else { // hidden cell
    	    	cell.style.opacity = 0;
    	    	cell.style.transform = 'none';
    	    }
    	}
    	let scene = this.shadowRoot.querySelector("div.scene");
    	let bordered = "bordered";
    	if (this.border) {
    		scene.classList.add(bordered);
    	} else {
    		scene.classList.remove(bordered);
    	}
    	if (this.width && this.height) {
    		scene.style.cssText += "width: "+ this.width + "; height: " + this.height;
    	}
    	this.rotate();
    }
    initNavi() {
		if (this.isHorizontal) {
			this.naviPrev = "carousel-navi-horizontal carousel-navi-horizontal-prev";
			this.naviNext = "carousel-navi-horizontal carousel-navi-horizontal-next";
		} else {
			this.naviPrev = "carousel-navi-vertical carousel-navi-vertical-prev";
			this.naviNext = "carousel-navi-vertical carousel-navi-vertical-next";
		}
    }
    correctLayoutForIE() {
    	for (var index = 0; index < this.cells.length; index++) { 
    		this.cells[index].style.cssText += "position: absolute; width: calc(100% - 20px); height: calc(100% - 20px); left: 10px; top: 10px; border: 1px solid #EFEFEF; display: -ms-flexbox; display: -webkit-flex"
        			+ "display: flex; -ms-flex-align: center; -webkit-align-items: center; -webkit-box-align: center; align-items: center; justify-content: center;";
    	};
    }
    // Observers
	_onOrienationChanged() {
		this.rotateFn = this.isHorizontal ? 'rotateY' : 'rotateX';
		this.naviStyle = this.isHorizontal ? 'carousel-navi-horizontal' : 'carousel-navi-vertical';
		this.iconPrev = this.isHorizontal ? 'vaadin:caret-left' : 'vaadin:caret-down';
		this.iconNext = this.isHorizontal ? 'vaadin:caret-right' : 'vaadin:caret-up';
		this.initNavi();
	}
    _onCenterStageChanged() {
        this.selectedComponentIndex = this.selectedIndex % this.cellCount;
        if (this.selectedComponentIndex < 0) {
            this.selectedComponentIndex = this.cellCount + this.selectedComponentIndex;
        }
     }
	// Lifecycle
	afterServerUpdate() {
        this.carousel = this.shadowRoot.querySelector("div.carousel");
		this.cells = this.shadowRoot.querySelector("slot").assignedNodes();
		this.cellCount = this.cells.length;
		this.cellWidth = this.carousel.offsetWidth;
		this.cellHeight = this.carousel.offsetHeight;
		this.initNavi();
		this.initCarousel();
		let userAgent = navigator.userAgent;
		let ie = userAgent.indexOf("MSIE") > -1 || userAgent.indexOf("rv:") > -1 || userAgent.indexOf("Edge") > -1;
		if (ie) {
			this.correctLayoutForIE()
		}
		if (this.autoplay && this.autoInterval === 0) {
    		this.autoInterval = setInterval(() => this.next(), this.duration);
    	} else {
    		clearInterval(this.autoInterval);
    		this.autoInterval = 0;
    	}
    }
}
customElements.define(Carousel.is, Carousel);